<?php

namespace FlowControl\ListView\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MakeListView extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'flowcontrol:listview';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a ListView class.';

    protected $type = 'ListView class';

    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function fire()
    {
        $input = $this->getNameInput();
        $noListViewName = str_replace('ListView', '', $input);
        $singular = str_singular($noListViewName);
        $plural = str_plural($singular);
        $singularListView = $singular . 'ListView';
        $pluralListView = $plural . 'ListView';

        $name = $this->parseName($pluralListView);

        $path = $this->getPath($name);

        if ($this->alreadyExists($pluralListView)) {
            $this->error($this->type.' already exists!');

            return false;
        }

        $this->makeDirectory($path);

        $this->files->put($path, $this->buildClass($name));

        $this->info($this->type.' created successfully.');

        $snake = snake_case($singular, '-');

        $name = str_plural($snake);

        $from = __DIR__ . '/stubs/list.blade.stub';

        $viewPath = config('flowcontrol.viewPath');

        if(strlen($viewPath) > 0) {
            $viewPath .= '/';
        }

        $targetPath = resource_path("views/{$viewPath}{$name}/");
        $fileName = 'list.blade.php';

        if( $this->files->exists($targetPath . $fileName) )
        {
            $this->error("File views/{$viewPath}{$name}/{$fileName} already exists!");
            return;
        }

        if( !$this->files->isDirectory($targetPath) )
        {
            $this->files->makeDirectory($targetPath, 0755, true);
        }

        if( $this->files->copy($from, $targetPath . $fileName) )
        {
            $this->info("Created views/{$viewPath}{$name}/{$fileName}");
            return;
        }

        $this->error("Could not create views/{$viewPath}{$name}/{$fileName}");
    }


    protected function replaceClass($stub, $name)
    {
        $stub = parent::replaceClass($stub, $name);

        $noListViewName = str_replace('ListView', '', $this->getNameInput());
        $nameSingular = str_singular($noListViewName);

        $dummyRoute = config('flowcontrol.prefix') . '.' . str_plural(
            strtolower( snake_case( $nameSingular, '-' ) )
        );
        $stub = str_replace('dummyroute', $dummyRoute, $stub);

        return $stub;
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/ListView.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Http\ListViews';
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the ListView class.'],
        ];
    }
}