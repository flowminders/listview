<?php

namespace FlowControl\ListView\Formatters;

use FlowControl\ListView\Contracts\Formatter;

class YesNoFormatter implements Formatter
{
    public function format($value)
    {
        return (bool)$value ? '<span class="label label-success">' . trans('flowcontrol-listview::formatters.yesno.yes') . '</span>' : '<span class="label label-danger">' . trans('flowcontrol-listview::formatters.yesno.no') . '</span>';
    }
}
