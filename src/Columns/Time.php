<?php

namespace FlowControl\ListView\Columns;


class Time extends Column
{
    public function getValue()
    {
        return $this
            ->toCarbon(parent::getValue())
            ->format(
                $this->getFormat(config('flowcontrol.listview.time_format'))
            );
    }
}