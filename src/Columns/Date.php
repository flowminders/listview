<?php

namespace FlowControl\ListView\Columns;

class Date extends Column
{
    public function getValue()
    {
        return $this
            ->toCarbon(parent::getValue())
            ->format(
                $this->getFormat(config('flowcontrol.listview.date_format'))
            );
    }
}