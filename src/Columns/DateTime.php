<?php

namespace FlowControl\ListView\Columns;


class DateTime extends Column
{
    public function getValue()
    {
        return $this
            ->toCarbon(parent::getValue())
            ->format(
                $this->getFormat(config('flowcontrol.listview.datetime_format'))
            );
    }
}