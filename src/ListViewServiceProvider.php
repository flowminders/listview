<?php

namespace FlowControl\ListView;


use FlowControl\Filters\FiltersServiceProvider;
use FlowControl\ListView\Commands\MakeListView;
use Illuminate\Support\ServiceProvider;

class ListViewServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/Views', 'flowcontrol/listview');

        $this->loadTranslationsFrom(__DIR__ . '/Lang', 'flowcontrol-listview');

        $this->publishes([
            __DIR__ . '/Config/flowcontrol.listview.php' => config_path('flowcontrol.listview.php')
        ], 'config');

        $this->publishes([
            __DIR__ . '/Views' => resource_path('views/vendor/flowcontrol/listview')
        ], 'views');

        $this->publishes([
            __DIR__ . '/Lang' => resource_path('lang/vendor/flowcontrol-listview')
        ]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(FiltersServiceProvider::class);

        $this->mergeConfigFrom(__DIR__ . '/Config/flowcontrol.listview.php', 'flowcontrol.listview');

        $this->commands([
            MakeListView::class,
        ]);
    }
}
