<?php

namespace FlowControl\ListView\Contracts;

interface Formatter
{
    public function format($value);
}